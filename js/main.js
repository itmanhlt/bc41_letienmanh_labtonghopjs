//bai 1
function bai1() {
  var contentHTML = "";
  for (var i = 1; i <= 100; i++) {
    contentHTML += `${i} `;
    if (i % 10 == 0) {
      contentHTML += `<br />`;
    }
  }
  document.getElementById("kqBai1").innerHTML = contentHTML;
}

//bai 2
let arr = [];
function nhapSo() {
  let number = document.getElementById("nhapBai2").value * 1;
  arr.push(number);
  document.getElementById("arrBai2").innerHTML = `[${arr}]`;
}

function checkSoNguyenTo(n) {
  var check = true;
  if (n < 2) {
    check = false;
  } else {
    for (var j = 2; j < n - 1; j++) {
      if (n % j == 0) {
        check = false;
        break;
      }
    }
  }
  return check;
}

function bai2() {
  let result = "";
  for (var i = 0; i < arr.length; i++) {
    var check = checkSoNguyenTo(arr[i]);
    if (check) {
      result += arr[i] + ", ";
    }
  }
  document.getElementById("kqBai2").innerHTML = `kết quả: ${result}`;
}
//bai 3
function bai3() {
  let n = document.getElementById("nhapBai3").value * 1;
  let s = 0;
  let kq = 0;
  for (var i = 2; i <= n; i++) {
    s += i;
  }
  kq += s + 2 * n;
  document.getElementById("kqBai3").innerHTML = kq;
}

//bai 4
function bai4() {
  let n = document.getElementById("nhapBai4").value * 1;
  let kq = "";
  for (let i = 0; i <= n; i++) {
    if (n % i == 0) {
      kq += i + ", ";
    }
  }
  document.getElementById("kqBai4").innerHTML = `Ước số của ${n} là: ${kq}`;
}

//bai 5
function bai5() {
  let n = document.getElementById("nhapBai5").value * 1;
  let kq = "";
  if (n < 0) {
    alert("n phải là số nguyên dương");
  } else {
    let number = "" + n;
    var arr = [...number];
    for (let i = arr.length - 1; i >= 0; i--) {
      kq += arr[i];
    }
  }
  document.getElementById("kqBai5").innerHTML = `Kết quả: ${kq}`;
}
//bai 6
function bai6() {
  var sum = 0;
  for (var i = 1; i < 100; i++) {
    sum += i;
    if (sum < 100) {
      document.getElementById("kqBai6").innerHTML = `x = ${i}`;
    }
  }
}
//bai 7
function bai7() {
  var n = document.getElementById("nhapBai7").value * 1;
  var kq = 0;
  var contentHTML = "";
  for (var i = 1; i <= 10; i++) {
    contentHTML += `${n} x ${i} = ${n * i}<br />`;
  }
  document.getElementById("kqBai7").innerHTML = contentHTML;
}
// bai 8
function bai8() {
  var players = [[], [], [], []];
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  var count = 0;
  cards.forEach((item) => {
    if (count > 3) {
      count = 0;
    }
    players[count].push(item);
    count++;
  });
  document.getElementById(
    "kqBai8"
  ).innerHTML = `player1 = [${players[0]}]<br />player2 = [${players[1]}]<br />player3 = [${players[2]}]<br />player4 = [${players[3]}]<br />`;
}
//bai 9
/*
x la gà
y la chó
x + y = m;
2x + 4y = n;
x + 3y = n - m;
x + 3(m -x) = n -m;
x + 3m - 3x = n -m;
2x = 4m -n
x = (4m - n)/2
 */
function bai9() {
  var m = document.getElementById("nhapBai9m").value * 1;
  var n = document.getElementById("nhapBai9n").value * 1;
  var x = 0;
  var y = 0;
  x = (4 * m - n) / 2;
  y = m - x;
  document.getElementById(
    "kqBai9"
  ).innerHTML = `Số con gà: ${x} con <br />Số con chó: ${y} con`;
}
//bai 10
function bai10() {
  var gio = document.getElementById("nhapBai10Gio").value * 1;
  var phut = document.getElementById("nhapBai10Phut").value * 1;
  var kq = Math.abs(30 * gio - (330 / 60) * phut - 360);
  document.getElementById(
    "kqBai10"
  ).innerHTML = `<p>Góc lệch giữa kim giờ và kim phút là: ${kq} độ</p>`;
}
